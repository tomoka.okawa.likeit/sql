SELECT category_name,SUM(item_price)AS total_price
FROM item a 
INNER JOIN item_category b
ON a.category_id = b.category_id
GROUP BY category_name
ORDER BY total_price DESC;
